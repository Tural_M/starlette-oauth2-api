import unittest
import json
import datetime

import jose.jwt
from starlette.applications import Starlette
from starlette.testclient import TestClient

from starlette_oauth2_api import AuthenticateMiddleware, _get_keys


def case_1(**kwargs):
    key = 'not-secret'
    audience = 'audience'
    issuer = 'https://example.com/'

    def _get_keys(path):
        return key

    app = Starlette()

    if 'get_keys' not in kwargs:
        kwargs['get_keys'] = _get_keys

    app.add_middleware(AuthenticateMiddleware,
        providers={
            'custom': {
                'keys': f'https://example.com/tenant-id/v2.0/jwks',
                'issuer': issuer,
                'audience': audience,
            }
        },
        **kwargs,
    )
    return app, key, audience, issuer


def case_2(keys):
    audience = 'audience'
    issuer = 'https://example.com/'

    app = Starlette()
    app.add_middleware(AuthenticateMiddleware,
        providers={
            'custom': {
                'keys': keys,
                'issuer': issuer,
                'audience': audience,
            }
        },
    )
    return app, audience, issuer


class MiddlewareCheck:
    def __init__(self, app, storage):
        self._app = app
        self._storage = storage

    async def __call__(self, scope, receive, send):
        self._storage['scope'] = scope
        return await self._app(scope, receive, send)


class TestCase(unittest.TestCase):
    def test_no_header(self):
        app, key, audience, issuer = case_1()

        client = TestClient(app)
        response = client.get('/')

        self.assertEqual(response.status_code, 400)

    def test_wrong_header(self):
        app, key, audience, issuer = case_1()

        client = TestClient(app)
        response = client.get('/', headers={'authorization': 'Baa '})

        self.assertEqual(response.status_code, 400)

    def test_all_good(self):
        app, key, audience, issuer = case_1()

        client = TestClient(app)
        token = jose.jwt.encode({
            'exp': datetime.datetime.utcnow() + datetime.timedelta(seconds=3600),
            'iat': datetime.datetime.utcnow(),
            'aud': audience,
            'iss': issuer,
        }, key)
        response = client.get('/', headers={'authorization': f'Bearer {token}'})

        self.assertEqual(response.status_code, 404)

    def test_keys_as_dict(self):
        key = 'not-secret'
        keys = {'keys': [key]}
        app, audience, issuer = case_2(keys)

        client = TestClient(app)

        token = jose.jwt.encode({
            'exp': datetime.datetime.utcnow() + datetime.timedelta(seconds=3600),
            'iat': datetime.datetime.utcnow(),
            'aud': audience,
            'iss': issuer,
        }, key)
        response = client.get('/', headers={'authorization': f'Bearer {token}'})
        self.assertEqual(response.status_code, 404)

    def test_check_claims(self):
        app, key, audience, issuer = case_1()

        storage = {}
        app.add_middleware(MiddlewareCheck, storage=storage)

        client = TestClient(app)
        claims = {
            'exp': datetime.datetime.utcnow() + datetime.timedelta(seconds=3600),
            'iat': datetime.datetime.utcnow(),
            'aud': audience,
            'iss': issuer,
            'custom': 'a custom claim'
        }
        token = jose.jwt.encode(claims, key)
        response = client.get('/', headers={'authorization': f'Bearer {token}'})
        self.assertEqual(response.status_code, 404)

        self.assertEqual(storage['scope']['oauth2-claims'], claims)

    def test_ignore_at_hash(self):
        """Explicitly test that we ignore the ``at_hash`` of the jwt."""
        app, key, audience, issuer = case_1()

        client = TestClient(app)
        claims = {
            'exp': datetime.datetime.utcnow() + datetime.timedelta(seconds=3600),
            'iat': datetime.datetime.utcnow(),
            'aud': audience,
            'iss': issuer,
        }
        token = jose.jwt.encode(claims, key, access_token="test_access_token")
        response = client.get('/', headers={'authorization': f'Bearer {token}'})

        self.assertEqual(response.status_code, 404)

    def test_wrong_key(self):
        app, _, audience, issuer = case_1()

        client = TestClient(app)
        token = jose.jwt.encode({
            'exp': datetime.datetime.utcnow() + datetime.timedelta(seconds=3600),
            'iat': datetime.datetime.utcnow(),
            'aud': audience,
            'iss': issuer,
        }, 'wrong-key')
        response = client.get('/', headers={'authorization': f'Bearer {token}'})

        self.assertEqual(response.status_code, 401)
        self.assertEqual(response.json(), {'message': {'custom': 'Signature verification failed.'}})

    def test_expired(self):
        app, key, audience, issuer = case_1()

        client = TestClient(app)
        token = jose.jwt.encode({
            'exp': datetime.datetime.utcnow() - datetime.timedelta(seconds=1800),
            'iat': datetime.datetime.utcnow() - datetime.timedelta(seconds=3600),
            'aud': audience,
            'iss': issuer,
        }, key)
        response = client.get('/', headers={'authorization': f'Bearer {token}'})

        self.assertEqual(response.status_code, 401)
        self.assertEqual(response.json(), {'message': 'Signature has expired.'})

    def test_wrong_audience(self):
        app, key, audience, issuer = case_1()

        client = TestClient(app)
        token = jose.jwt.encode({
            'exp': datetime.datetime.utcnow() + datetime.timedelta(seconds=3600),
            'iat': datetime.datetime.utcnow(),
            'aud': 'wrong-audience',
            'iss': issuer,
        }, key)
        response = client.get('/', headers={'authorization': f'Bearer {token}'})

        self.assertEqual(response.status_code, 401)
        self.assertEqual(response.json(), {'message': {'custom': 'Invalid audience'}})

    def test_wrong_issuer(self):
        app, key, audience, issuer = case_1()

        client = TestClient(app)
        token = jose.jwt.encode({
            'exp': datetime.datetime.utcnow() + datetime.timedelta(seconds=3600),
            'iat': datetime.datetime.utcnow(),
            'aud': audience,
            'iss': 'wrong-issuer',
        }, key)
        response = client.get('/', headers={'authorization': f'Bearer {token}'})

        self.assertEqual(response.status_code, 401)
        self.assertEqual(response.json(), {'message': {'custom': 'Invalid issuer'}})

    def test_wrong_signature(self):
        app, key, audience, issuer = case_1()

        client = TestClient(app)
        token = jose.jwt.encode({
            'exp': datetime.datetime.utcnow() + datetime.timedelta(seconds=3600),
            'iat': datetime.datetime.utcnow(),
            'aud': audience,
            'iss': issuer,
        }, key) + "a"  # make the signature wrong
        response = client.get('/', headers={'authorization': f'Bearer {token}'})

        self.assertEqual(response.status_code, 401)
        self.assertEqual(response.json(), {'message': {'custom': 'Signature verification failed.'}})

    def test_public_path(self):
        app, key, audience, issuer = case_1(public_paths={'/'})

        client = TestClient(app)
        response = client.get('/')

        self.assertEqual(response.status_code, 404)

    def test_default_get_keys(self):
        app, key, audience, issuer = case_1(public_paths={'/'}, get_keys=None)

    def test_wrong_provider(self):
        with self.assertRaises(ValueError) as e:
            AuthenticateMiddleware(None, 
                providers={
                    'custom': {
                        'keys': f'https://example.com/tenant-id/v2.0/',
                        'audience': 'audience',
                    }
                },
            )
        self.assertIn('"custom" is missing {\'issuer\'}.', str(e.exception))

    def test_get_keys(self):
        keys = _get_keys('https://login.microsoftonline.com/common/discovery/v2.0/keys')
        self.assertIn('keys', keys)

    def test_wrong_configuration(self):
        with self.assertRaises(ValueError):
            # not https
            case_2('http://example.com')
